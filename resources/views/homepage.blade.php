<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style media="screen">
      body{
      background: url('{{ asset('template_madan/images/madan1.jpg') }}') no-repeat;
      background-size: cover;

      }
      .box{
        position: absolute;
        width: 750px;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
      }
    </style>
  </head>
  <body>
    <div class="container">
        <div class="box mb-100" style="background:white;">
            <center>
              <h3 class="cool-md-12" style="background:white; width:500px;">Pemilihan Mahasiswa Teladan IT Del</h3>
            </center>
          <p >
            <center>
            Mahasiswa teladan merupakan salah satu mahasiswa yang menjadi panutan untuk mahasiswa lain.
            Mahasiswa teladan memiliki sikap dan perilaku yang patut untuk dicontoh oleh mahasiswa lainnya.
            Di Institut Teknologi Del (IT Del) pemilihan mahasiswa teladan diadakan setiap 1 (satu) tahun sekali.
            </center>
          </p>
          <center>
            <a href="/dimx_dim" class="btn btn-primary">Jalankan Aplikasi</a>
          </center>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
